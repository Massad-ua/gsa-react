var router = require('express').Router();
var mocks = require('./mock');
var assign = require('object-assign');

router.get('/article', function (req, res, next) {
    var articles = mocks.articles.map(function (article) {
            return assign({}, article, {
                text: undefined
            })
        }),
        limit = Number(req.query.limit) || articles.length,
        offset = Number(req.query.offset) || 0;

    res.json(articles.slice(offset, limit + offset))
});

router.get('/article/:id', function (req, res, next) {
    var article = mocks.articles.filter(function (article) {
        return article.id == req.params.id
    })[0];
    if (article) return res.json(article);

    res.status(404).json({error: "not found"});
});

router.post('/article', function (req, res, next) {
    var body = req.body;
    var article = {
        text: body.text,
        id: Date.now().toString(),
        user: body.user,
        date: new Date()
    };
    mocks.articles.push(article);
    res.json(article)
});
/******/


router.get('/product', function (req, res, next) {
	var products = mocks.products.map(function (article) {
			return assign({}, article, {
				description: undefined
			})
		}),
		limit = Number(req.query.limit) || products.length,
		offset = Number(req.query.offset) || 0;

	res.json(products.slice(offset, limit + offset))
});

router.get('/product/:id', function (req, res, next) {
	var product = mocks.products.filter(function (product) {
		return product.id == req.params.id
	})[0];
	if (product) return res.json(product);

	res.status(404).json({error: "not found"});
});

router.post('/product', function (req, res, next) {
	var body = req.body;
	var product = {
		description: body.text,
		id: Date.now().toString(),
		user: body.user,
		date: new Date()
	};
	mocks.products.push(product);
	res.json(product)
});



router.get('/manufacturer', function (req, res, next) {
	var aid = req.query.product;
	if (aid) {
		var product = mocks.products.find(function(product) {
			return product.id == aid
		})
		return res.json((product.idManufacturer || []).map(function(id) {
		    console.log(product.idManufacturer);
			return mocks.manufacturers.find(function(manufacturer) {
				return manufacturer.id == id
			})
		}))
	}

	var limit = Number(req.query.limit) || mocks.manufacturers.length,
		offset = Number(req.query.offset) || 0;
	res.json({
		total: mocks.manufacturers.length,
		records: mocks.manufacturers.slice(offset, limit + offset)
	})
});

router.post('/manufacturer', function (req, res, next) {
	var manufacturer = {
		id : Date.now().toString(),
		name : req.body.name,
		date: new Date(),
	};
	mocks.manufacturers.push(manufacturer);
	res.json(manufacturer)
});

router.post('/report', function (req, res) {
	res.json({})
});


/************************************************/
router.get('/comment', function (req, res, next) {
    var aid = req.query.article;
    if (aid) {
        var article = mocks.articles.find(function(article) {
            return article.id == aid
        })
        return res.json((article.comments || []).map(function(id) {
            return mocks.comments.find(function(comment) {
                return comment.id == id
            })
        }))
    }

    var limit = Number(req.query.limit) || mocks.comments.length,
        offset = Number(req.query.offset) || 0;
    res.json({
        total: mocks.comments.length,
        records: mocks.comments.slice(offset, limit + offset)
    })
});

router.post('/comment', function (req, res, next) {
    var comment = {
        id : Date.now().toString(),
        text : req.body.text,
        date: new Date(),
        user: req.body.user,
        article : req.body.article
    };
    mocks.comments.push(comment);
    res.json(comment)
});

router.post('/report', function (req, res) {
    res.json({})
})

module.exports = router;
