import { manufacturers as defaultManufacturers }  from '../fixtures'
import {DELETE_MANUFACTURER, INCREMENT } from '../constants'
import { arrToMap, mapToArr }from '../helpers'
import {Map, Record} from 'immutable'

const manufacturersRecord = Record({
	id: undefined,
	name: undefined,
	src: undefined
});


export default (manufacturersState = defaultManufacturers, action) => {
	const {type,  payload } = action;

	switch(type){
		case DELETE_MANUFACTURER:
			return manufacturersState.delete(payload.id);
			return manufacturersState.filter(function(el,index,array) {
				return el.id !== payload.id
			})
	}

	return arrToMap(manufacturersState, manufacturersRecord);
}