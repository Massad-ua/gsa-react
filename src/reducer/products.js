import { products }  from '../fixtures'
import { manufacturers }  from '../fixtures'
import {arrToMap} from '../helpers'
import {DELETE_MANUFACTURER, INCREMENT, LOAD_ALL_ARTICLES, LOAD_ALL_PRODUCTS, START, SUCCESS} from '../constants'
import {Map, Record, OrderedMap } from 'immutable'

const ReducerState = Record({
	loading: false,
	loaded: false,
	entities: new OrderedMap({})
});


const productsRecord  =  Record({
	id: "",
	name: "",
	idManufacturer: "",
	vendorCode: "",
	src: "",
	availability: "",
	state: "",
	price: ""
});

const defaultState = new ReducerState();

export default (productsState = defaultState, action) => {
	const {type, response} = action;

	switch (type){
		case  LOAD_ALL_PRODUCTS + START:
			return productsState.set('loading', true);

		case LOAD_ALL_PRODUCTS + SUCCESS:
			return productsState
				.set('entities', arrToMap(response, productsRecord))
				.set('loading', false)
				.set('loaded', true);
	}

	// console.log(productsState);
	return productsState;
}