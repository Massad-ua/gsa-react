import {combineReducers} from 'redux'
import counterReducer  from './counter'
import manufacturers from './manufacturers'
import products from './products'
import filters from './filters'
import sections from './sections'
import news from './news'
import manufacturersProducts from './manufacturersProducts'
import sliders from './sliders'

export default combineReducers({
	count: counterReducer,
	manufacturers,
	manufacturersProducts,
	products,
	filters,
	sections,
	news,
	sliders
});