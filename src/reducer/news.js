import {articlesNews as defaultNews} from '../fixtures'
import { arrToMap } from '../helpers'
import {Map, Record} from 'immutable'


const newsRecord = Record({
	id: undefined,
	author: undefined,
	text: undefined,
	src: undefined
});

const newsMap = arrToMap(defaultNews, newsRecord);


// console.log("newsMap", newsMap);

export default (newsState = newsMap, action) => {
	const {type, payLoad} = action;
	return newsState;
}
