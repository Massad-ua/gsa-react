import {arrToMap} from '../helpers'
import {LOAD_SLIDERS, SUCCESS} from '../constants'
import {Map, Record, OrderedMap } from 'immutable'


const ReducerState = Record({
	loading: false,
	entities: new OrderedMap({})
});

const sliderRecord  =  Record({
	id: "",
	src: ""
});


const defaultState =  new ReducerState();

export default (sliderState = defaultState, action) => {
	const {type, responseSlider} = action;
	switch (type){
		case  LOAD_SLIDERS + SUCCESS:
			return sliderState.set('loading', true)
								.set('entities', arrToMap(responseSlider, sliderRecord));
	}
	// console.log(productsState);
	return sliderState;
}