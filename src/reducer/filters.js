import { CHANGE_SELECTION} from '../constants'

//state по умолчанию
const defaultFilters ={
	selected:[],
};

export default(filters = defaultFilters, action) => {
	const  { type, payload} = action;

	switch(type){
		case CHANGE_SELECTION:
			// let c = { ...filters, selected: payload.selected };
			return { ...filters, selected: payload.selected };
	}
	return filters
}