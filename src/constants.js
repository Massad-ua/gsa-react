export const INCREMENT = 'INCREMENT';
export const DELETE_MANUFACTURER = 'DELETE_MANUFACTURER';
export const CHANGE_SELECTION = 'CHANGE_SELECTION';

export const LOAD_ALL_ARTICLES = 'LOAD_ALL_ARTICLES';
export const LOAD_ALL_PRODUCTS = 'LOAD_ALL_PRODUCTS';

export const LOAD_PRODUCT = 'LOAD_PRODUCT';

export const  START = "_START";
export const  SUCCESS = "_SUCCESS";
export const  FAIL = "_FAIL";

export const LOAD_FIREBASE = "LOAD_FIREBASE";
export const LOAD_SLIDERS = "LOAD_SLIDERS";


