import { createSelector } from 'reselect'
import { mapToArr } from '../helpers'

const filtersGetter =  state => state.filters;
// const productsGetter =  state => state.prodsManufacturers.products;
const productsGetter =  state => state.products.entities;

const newsGetter = state => state.news;
const idGetter = (state, props) => props.id;


export const filtrateProductsSelector = createSelector( productsGetter, filtersGetter, (products, filters) => {
	const {selected} = filters;
	return  mapToArr(products).filter( product => {
		return (!selected.length || selected.includes(product.id))
	});
});


export const newsSelectorFactory = () => createSelector(newsGetter, idGetter, (news, id) => {
	// console.log('---', 'getting news ID', id);
	// console.log('---', 'getting news Object', news.get("news9"));

	return news.get(id)
});


// Селектор без исрользования createSelector
// export function filtrateProducts({filters, products}) {
// 	const {selected} = filters;
// 	return  products.filter( product => {
// 		return (!selected.length || selected.includes(product.id))
// 	});
// }




