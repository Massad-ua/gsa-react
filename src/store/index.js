import {createStore, applyMiddleware} from 'redux'
import reducer from '../reducer'
import logger from '../middlewares/logger'
import nextMiddleware from '../middlewares/nextMiddleware'
import api from '../middlewares/api'
import slidersApi from '../middlewares/slidersApi'
import thunk from 'redux-thunk'

const enhancer = applyMiddleware(thunk,logger, api, nextMiddleware, slidersApi);
const store = createStore(reducer, {}, enhancer);

// dev only
window.store = store;

export default store