import React, { Component }  from 'react'
import App from './app'
import store from '../store'
import {Provider} from 'react-redux'

export default class Root extends  Component{
	render(){
		return(
			<Provider store={store}>
				<App/>
			</Provider>
		)
	}
}