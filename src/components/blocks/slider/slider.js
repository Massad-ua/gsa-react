import React, { Component } from 'react'
import {findDOMNode} from 'react-dom'
import {connect} from 'react-redux'
import {loadSliderElements} from '../../../actions'
import PropTypes from 'prop-types'
import slick from 'slick-carousel'
import '../../../../node_modules/slick-carousel/slick/slick.css'
import '../../../../node_modules/slick-carousel/slick/slick-theme.css'
import $ from "jquery"
import { mapToArr } from '../../../helpers'

class Slider extends Component{

	constructor(props){
		super(props);
		this.sliderElements = [];
		this.sliderContainer = null;
	}

	componentDidMount(){
		const { loading, loadSliderElements } =  this.props;
		if(!loading) loadSliderElements();
	}

	render(){
		const {sliders} =  this.props;
		this.sliderElements  = (mapToArr(sliders)).map(function(el, index, arr) {
			return(
				<div key = {el.id} className="slide-item">
					<img src = {el.src} className="slide-image"/>
				</div>
				)
		});
		return(
			<div id="sliderMain" className="container" ref={this.sliderInit}>
				{this.sliderElements}
			</div>
		)
	}

	sliderInit = ref => {
		setTimeout(function () {
			$(ref).slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		},100)
	};
}

export default connect((state) => {
	return{
		sliders: state.sliders.entities,
		loading: state.sliders.loading
	}
},{loadSliderElements})(Slider)