import React, { Component} from 'react'
import ChosenItem from '../../../decorators/chosenItem'
import {MainPage} from '../../pages/index'
import {CatalogPage} from '../../pages/index'
import {DeliveryPage} from '../../pages/index'
import {ContactPage} from '../../pages/index'
import {AboutPage} from '../../pages/index'

import { HashRouter as Router, Link, NavLink } from  'react-router-dom'


class Menu extends Component {
	constructor(props){
		super(props);

		// this.elemList = {};
		// console.log(this.props);
	}
	render(){
		return(
			<nav className="nav-container">
				<div className="container">
					<div className="side-line-left side-line"></div>
					<ul className="flexible menu-wrapper ">
						<li className="menu-item">
							<NavLink to="/main" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="01"  className={this.getClassName("01")}>
								Главная
							</NavLink>
						</li>

						<li className="menu-item">
							<NavLink to="/catalog" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="02" className={this.getClassName("02")} >
								Каталог
							</NavLink>
						</li>

						<li className="menu-item">
							<NavLink to="/delivery" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="03" className={this.getClassName("03")}>
								Доставка
							</NavLink>
						</li>

						<li className="menu-item">
							<NavLink to="/about" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="04" className={this.getClassName("04")}>
								О компании
							</NavLink>
						</li>

						<li className="menu-item">
							<NavLink to="/contact" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="05" className={this.getClassName("05")}>
								Контакты
							</NavLink>
						</li>

						<li className="menu-item">
							<NavLink to="/admin" activeStyle={{color: '#049be5' }} onClick={ this.props.chooseItem} data-item="06" className={this.getClassName("06")}>
								Админка
							</NavLink>
						</li>
					</ul>
					<div className="side-line-right side-line"></div>
				</div>
			</nav>
		)
	}

	getClassName(arg){
		if(arg === this.props.elem ){
			return "active"
		}
		else {
			return ""
		}
	}
	// getElem = (ref)=> {
	// 	this.elemList[ref.dataset.item] = ref.dataset.item;
	// }

}

export default ChosenItem(Menu);