import Header from './header/header'
import Menu from './menu/menu'
import Search from './search/search'
import Slider from './slider/slider'
import Validation from './validation/validation'

export {
	Header,
	Menu,
	Search,
	Slider,
	Validation
}