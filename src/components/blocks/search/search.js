import React, { Component} from 'react'


export default class Search extends Component {
	constructor(props){
		super(props)
	}

	render(){
		return(
			<div className="search-container">
				<div className="container flexible space-between align-center">
					<img src="/src/asset/images/logo.jpg" className="logo-icon" alt=""/>
					<form className="search-wrapper">
						<input type="text" className="search-input " id="searchInput"/>
						<button type="submit" className="fas fa-search color-blue"></button>
					</form>

					<div className="contact-item">
						<i className="fas fa-phone-volume"></i>
						<span className="text">+7 924 722-22-40</span>
					</div>

					<div className="contact-item">
						<i className="fas fa-envelope"></i>
						<span className="text">gsatruckparts@gmail.com</span>
					</div>

					<div className="download-wrapper flexible flex-column">
						<a href="javascript: void(0);" className="color-blue">
							<i className="fas fa-clipboard-list"></i>
							<span className="text">Скачать прайс-лист</span>
						</a>

						<a href="javascript: void(0);" className="color-blue">
							<i className="fas fa-map-marker-alt"></i>
							<span className="text">Адреса складов</span>
						</a>

					</div>
				</div>
			</div>
		)
	}
}
