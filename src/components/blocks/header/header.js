import React, { Component} from 'react'
import './header.css'
import PropTypes from 'prop-types'
import Menu from '../menu/menu'
import Search from '../search/search'
import Slider from '../slider/slider'
import {manufacturers} from '../../../fixtures'

class Header extends Component{

	constructor(props){
		super(props);
	}

	static defaultProps = {
		comments: []
	};

	render(){
		return(
			<header>
				<div className="top-line-container flexible">
					<div className="container flexible space-between align-center">
						<span className="top-line-text">Продажа оригинальных запчастей на корейскую спецтехнику</span>
						<ul className="flexible space-between social-icon-wrapper">
							<li>
								<a href="">
									<i className="fab fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="">
									<i className="fab fa-vk"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<Search className="search-container"/>
				<Menu className="menu-container"/>
			</header>
		)
	}
}

export default Header;