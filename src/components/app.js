import React, { Component }  from 'react'
import './main.css'
// import '../vendor/fontawesome/css/fontawesome-all.css'
import Header from './blocks/header/header'
import {MainPage} from './pages/index'
// import {manufacturers} from '../fixtures'

import Products from '../components/routes/Products'

import {CatalogPage} from './pages/index'
import {DeliveryPage} from './pages/index'
import {ContactPage} from './pages/index'
import {AboutPage} from './pages/index'
import {AdminPage} from './pages/index'

import { HashRouter as Router, Route } from  'react-router-dom'

export default class App extends Component {

	constructor(props){
		super(props);
		this.state = {
			isOpen: false
		};
		this.key = 0;
	}

	render(){
		// console.log(this.props);
		if(this.state.isOpen) return <span> ura!</span>;
		return(
			<Router>
				<div>
					<Header/>
					<Route path ='/main'  component = {MainPage}/>
					<Route path ='/about'  component = {AboutPage}/>
					<Route path ='/catalog'  component = {CatalogPage}/>
					<Route path ='/delivery'  component = {DeliveryPage}/>
					<Route path ='/admin'  component = {AdminPage}/>
				</div>
			</Router>
		);
	}
	changeState(){
		this.key += 1;
		console.log(this.key);
		this.setState({});
		// this.setState({
		// 	isOpen: !this.state.isOpen
		// })
	}

}