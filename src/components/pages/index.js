import MainPage from './mainPage/mainPage'
import AboutPage from './AboutPage/AboutPage'
import CatalogPage from './CatalogPage/CatalogPage'
import ContactPage from './ContactPage/ContactPage'
import DeliveryPage from './DeliveryPage/DeliveryPage'
import AdminPage from './AdminPage/AdminPage'


export {
	MainPage,
	AboutPage,
	CatalogPage,
	ContactPage,
	DeliveryPage,
	AdminPage
}
