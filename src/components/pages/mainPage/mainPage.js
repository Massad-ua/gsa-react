import React, { Component }  from 'react'
import './mainPage.css'
// import {manufacturers} from '../../fixtures'
import {ManufacturersList, ProductList, Select} from '../../common/index';
import {products} from '../../../fixtures'
import Section from '../../common/section/section'
import {Slider} from '../../blocks/index'


export default class MainPage extends Component {

	constructor(props){
		super(props);
	}

	render(){
		return(
			<main>
				<div className="slider-container">
					<Slider className="container" />
				</div>
				<div className="manufacturers-container">
					<div className="container">
						<h2 className="title-h2">Производители</h2>
						<ManufacturersList/>
					</div>
				</div>

				<div className="products-container">

					<div className="container">
						<h2 className="title-h2">Новые поступления</h2>
						<Select />
						<ProductList />
					</div>
				</div>

				<div className="news-container">
					<div className="container">
						<h2 className="title-h2">Новости компании</h2>
						<Section />
					</div>
				</div>
			</main>

		);
	}
}