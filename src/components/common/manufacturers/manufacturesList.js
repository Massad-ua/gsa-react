import React, { Component }  from 'react'
import Manufacturer from './manufacturer'
import {connect} from 'react-redux'
import { arrToMap, mapToArr }from '../../../helpers'
import {NavLink, Route} from 'react-router-dom'
import Products from  '../../routes/Products'

class ManufacturersList extends Component {

	constructor(props){
		super(props);
	}

	render(){
		let {manufacturers} = this.props;
		manufacturers = mapToArr(manufacturers);

		const manufacturersElement = manufacturers.map( element =>
			<Manufacturer key={element.id} manufacturer = {element} />
		);

		// console.log('--- manufacturers element',manufacturersElement);
		// console.log('---',manufacturersElement);
		return(
			<div>
				<div className="flexible space-between manufacturer-wrapper" >
					{manufacturersElement}
				</div>
			</div>

		);
	}
}

export default connect((state) => {
	// console.log(state);
	return{
		manufacturers: state.manufacturers
	}
}
)(ManufacturersList)