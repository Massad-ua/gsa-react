import React, { Component }  from 'react'
import './manufacturer.css'
import {deleteManufacturer} from '../../../actions/'
import {connect} from 'react-redux'

class Manufacturer extends Component {

	constructor(props){
		super(props);
		this.state = {
			delete: 0
		}
	}
	// shouldComponentUpdate(nextProps, nextState){
	//
	// }

	render(){
		const {manufacturer} = this.props;
		return(
			<div className="manufacturer-wrapper">
				<div className="manufacturer-circle flexible centered">
					<img src={manufacturer.src} alt=""/>
				</div>
				<p className="manufacturer-name">{manufacturer.name}</p>
				<button onClick={this.deleteIt.bind(this)} className="btn">Hide me</button>
			</div>
		);
	}
	deleteIt(){
		const {deleteManufacturer, manufacturer} = this.props;
		deleteManufacturer(manufacturer.id);
		this.setState({
			delete: this.state.delete + 1
		});
	}
}

export default connect(null, {deleteManufacturer})(Manufacturer);