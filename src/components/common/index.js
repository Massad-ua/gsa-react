import Manufacturers from './manufacturers/manufacturer'
import ManufacturersList from './manufacturers/manufacturesList'
import Product from './products/product'
import ProductList from './products/productList'
import Select from './filters/Select'
import NewsList from './news/newsList'
import Loader from './loader/loader'


export {
	Manufacturers,
	ManufacturersList,
	Product,
	ProductList,
	Select,
	NewsList,
	Loader
}