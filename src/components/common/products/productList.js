import React, { Component }  from 'react'
import {connect} from 'react-redux'
import Product from './product'
import Products from  '../../routes/Products'
import {filtrateProducts, filtrateProductsSelector} from '../../../selectors/index'
import {loadAllProducts} from '../../../actions'
import Loader from "../loader/loader";
import {NavLink, Route} from 'react-router-dom'

class ProductList extends Component{

	constructor(props){
		super(props)
	}

	componentDidMount(){
		// console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		const {loaded, loading, loadAllProducts } =  this.props;
		if(!loaded || !loading) loadAllProducts();
		// console.log('-----props', this.props);
	}
	render() {
		// console.log('---', 'update product list');
		const {products, manufacturers, loading} =  this.props;
		if(loading) return <Loader/>;

		const productsElement = products.map(element =>
			<div  key={element.id}>

				<NavLink to={ `/product/${element.id}`}  activeStyle={{color: 'red'}} >
					<Product product={element} manufacturers={manufacturers}/>
				</NavLink>

			</div>
		);

		return (
			<div>
				<div className="flexible flex-wrap">
					{productsElement}
				</div>
			</div>
		)
	}
}

export default connect((state, ownProps) => {
	console.log(state);
	return {
		products: filtrateProductsSelector(state),
		manufacturers: state.manufacturersProducts,
		loading: state.products.loading,
		loaded: state.products.loaded
	}
},{loadAllProducts})(ProductList)
