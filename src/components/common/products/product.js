import React, { Component }  from 'react'
import './product.css'

class Product extends Component {

	constructor(props){
		super(props);
	}

	render(){
		const {product, manufacturers} = this.props;
		let manufacturersId = product.idManufacturer[0];

		// console.log('---', manufacturers);
		const nameManufacturer = (manufacturers.filter(element => element.id === manufacturersId))[0].name;

		return(
			<div className="catalog-box flexible flex-column">
				<div className="label-top"></div>
				<img src={product.src} alt=""/>
				<div>
					<hr/>
					<div>
						<h4>{product.name}</h4>
						<ul className="box-list">
							<li className="flexible">
								<span className="box-vendor">
									<span>Производитель</span>
									<hr className="line-box"/>
								</span>
								<span className="box-list-name">
									<span>{ nameManufacturer}</span>
								</span>
							</li>
						</ul>
					</div>
					<div>
						<select name="" id="">
							<option value="">Владивосток</option>
							<option value="">Рязань</option>
							<option value="">Пемза</option>
						</select>
					</div>
				</div>
			</div>
		);
	}
}

export default Product