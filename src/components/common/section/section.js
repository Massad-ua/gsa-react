import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {NewsList} from '../index'

class Section extends Component{

	static propTypes = {
		section: PropTypes.array,
	};

	constructor(props){
		super(props);
		this.state = {
			currentSection: this.props.sections[0]
		}
	}

	render(){
		const {sections} = this.props;

		const optionElement = sections.map(element =>
			<option value={element.idPage} key={element.idPage}>{element.idPage}</option>
		);
		return(
			<div>
				<select name="" id="" onChange={this.changeSelect.bind(this)}>
					{optionElement}
				</select>
				<div>
					<NewsList news = {this.state.currentSection.news} />
				</div>
			</div>
		)
	}
	changeSelect(e){
		const section = this.props.sections.filter((element) => element.idPage === e.target.value);

		this.setState({
			currentSection: section[0]
		})

	}
}

export default connect( (state) => {
	return {
		sections: state.sections
	}
} )(Section);
