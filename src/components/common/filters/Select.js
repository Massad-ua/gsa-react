import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'
import {connect} from 'react-redux'
import { changeSelection } from '../../../actions/'
import {arrToMap, mapToArr} from '../../../helpers'

import 'react-select/dist/react-select.css'

class SelectFilter extends Component {
	handleChange = selected => this.props.changeSelection(selected.map(option => option.value));
	render() {

		let { products, selected } = this.props;
		!products && (products = []);

		const options = products.map(product => ({
			label: product.name,
			value: product.id
		}));

		return <Select
			options={options}
			value={selected}
			multi={true}
			onChange={this.handleChange}
		/>
	}
}

export default connect(state =>({
	selected: state.filters.selected,
	products: mapToArr(state.products.entities)
}), {changeSelection})(SelectFilter)