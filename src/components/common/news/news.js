import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {newsSelectorFactory} from '../../../selectors/index'

class News extends Component{

	static propTypes = {
		news: PropTypes.shape({
			id: PropTypes.string.isRequired,
			author: PropTypes.string.isRequired,
			text: PropTypes.string.isRequired,
			src: PropTypes.string.isRequired
		}).isRequired,
	};

	constructor(props){
		super(props);
	}

	render(){
		const {news} = this.props;
		// console.log('--------NNNNNNNNNNNN',news);
		return(
			<div className="flexible">
				<img src={news.src} alt=""/>

				<div>
					<h3>{news.author}</h3>
					<span>{news.text}</span>
				</div>
			</div>
		)
	}
}

///Замыкание
const mapStateToProps = () => {
	const newsSelector = newsSelectorFactory();
	return (state, ownProps) => {
		return{
			news: newsSelector(state, ownProps)
		}
	}
};

export default connect(mapStateToProps)(News)