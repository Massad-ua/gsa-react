import React, { Component } from 'react'
import PropTypes from 'prop-types'
import News from './news'

export default class NewsList extends Component{

	static propTypes = {
		news: PropTypes.array.isRequired
	};

	constructor(props){
		super(props);
	}

	render(){
		const {news} = this.props;

		console.log('-----------NEWS', news);
		const articlesElement = news.map( id =>
			<News key={id} id={id} />
		);

		// console.log('-----NL', articlesElement);

		return(
			<div>
				{articlesElement}
			</div>
		)
	}
}