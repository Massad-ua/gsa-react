import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Loader extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return(
			<div>
				<h2>Loading...</h2>
			</div>
		)
	}
}