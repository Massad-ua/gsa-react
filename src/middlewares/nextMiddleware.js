export default store =>  next  => action =>{
	console.log('---', 'state before 2', store.getState());
	console.log('---', 'dispatching 2', action);
	next(action);
	console.log('---', 'state after 2', store.getState());
}