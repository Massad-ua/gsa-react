import { LOAD_SLIDERS ,SUCCESS} from '../constants'

export default  store => next => action => {
	const {callAPIslider, type, ...rest} = action;
	if(!callAPIslider && (type !== LOAD_SLIDERS )) return next(action);

	const xhr = new XMLHttpRequest();
	xhr.open('GET', callAPIslider , true);
	xhr.send();
	xhr.onreadystatechange = function () {
		if(xhr.readyState === 4 && xhr.status >= 200 ) {
			console.log((JSON.parse(xhr.response)).sliderElements);
			next({
				...action,
				type: type + SUCCESS,
				responseSlider: (JSON.parse(xhr.response)).sliderElements
			});
		}
		else {
			next(action);
		}

	}

}

