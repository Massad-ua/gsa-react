import React, {Component as ReactComponent} from 'react'
import PropTypes from 'prop-types'

export default (OriginalComponent) => class ChosenItem extends ReactComponent{
	state = {
		elem: null
	};

	render(){
		return <OriginalComponent  {...this.props}  {...this.state}  chooseItem = {this.chooseItem}/>
	}

	chooseItem = (e) =>{
		this.setState({
			elem: e.target.dataset.item === this.state.elem ? null : e.target.dataset.item
		})
	}
}


