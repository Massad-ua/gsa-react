import {DELETE_MANUFACTURER,
		INCREMENT,
		CHANGE_SELECTION,
		LOAD_ALL_PRODUCTS,
		LOAD_PRODUCT,
		START,
		LOAD_FIREBASE,
		LOAD_SLIDERS} from '../constants'

export function increment() {
	return {
		type: 'INCREMENT'
	}
}

export function deleteManufacturer(id){
	return{
		type: DELETE_MANUFACTURER,
		payload: {
			id
		}
	}
}

export function changeSelection(selected) {
	return {
		type: CHANGE_SELECTION,
		payload: {selected}
	}
}

export function loadAllProducts () {
	return {
		type: LOAD_ALL_PRODUCTS,
		callAPI: '/api/product'
	}
}

export function loadProduct(id){
	return (dispatch) => {
		dispatch({
			type: LOAD_PRODUCT + START,
			payload: { id }
		});
		setTimeout(() => {
			fetch(`/api/product/${id}`)
				.then( res => res.json())
				.then(response => dispatch({
					type: LOAD_PRODUCT + SUCCESS,
					payload: { id, response }
				}))
				.catch( error  => dispatch({
					type: LOAD_PRODUCT + FAIL,
					payload: {id, error}
				}))
		}, 1000)
	}
}

export function loadFromFirebase() {
	return {
		type: LOAD_FIREBASE + START
	}
}

export function loadSliderElements() {
	return {
		type: LOAD_SLIDERS,
		callAPIslider: '/src/data.json'
	}
}